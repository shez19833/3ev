<?php

namespace Tests\Feature\Fleets;

use App\Models\Fleet;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class IndexTest extends TestCase
{
    use DatabaseMigrations;

    public function test_i_can_see_all_fleets()
    {
        $fleets = Fleet::factory()->count(2)->create();

        $this->getJson('/api/fleets')
            ->assertOk()
            ->assertSee($fleets[0]->name)
            ->assertSee($fleets[1]->name);
    }

    public function test_i_can_search_fleets_by_name()
    {
        $fleets = Fleet::factory()->count(2)->create();

        $this->getJson('/api/fleets?name=' . $fleets[0]->name)
            ->assertSee($fleets[0]->name)
            ->assertDontSee($fleets[1]->name);
    }

    public function test_i_can_search_fleets_by_category()
    {
        $fleets = Fleet::factory()->count(2)->create();

        $this->getJson('/api/fleets?category_id=' . $fleets[0]->category_id)
            ->assertSee($fleets[0]->name)
            ->assertDontSee($fleets[1]->name);
    }

    public function test_i_can_search_fleets_by_status()
    {
        $fleets = Fleet::factory()->count(2)->create();

        $this->getJson('/api/fleets?status_id=' . $fleets[0]->status_id)
            ->assertSee($fleets[0]->name)
            ->assertDontSee($fleets[1]->name);
    }

}
