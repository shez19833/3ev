<?php

namespace Tests\Feature\Fleets;

use App\Models\Fleet;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ShowTest extends TestCase
{
    use DatabaseMigrations;

    public function test_i_can_see_a_fleet_details_if_it_exists()
    {
        $fleet = Fleet::factory()->create();

        // @todo would ideally include a JSON or assertSEE everything to
        // make sure its all there
        $this->getJson('/api/fleets/' . $fleet->id)
            ->assertOk()
            ->assertSee($fleet->name);
    }

    public function test_i_see_a_not_found_if_fleet_doesnt_exist()
    {
        // @todo would ideally include a JSON or assertSEE everything to
        // make sure its all there
        $this->getJson('/api/fleets/222')
            ->assertNotFound();
    }
}
