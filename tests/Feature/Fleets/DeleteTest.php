<?php

namespace Tests\Feature\Fleets;

use App\Models\Fleet;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class DeleteTest extends TestCase
{
    use DatabaseMigrations;

    public function test_i_can_delete_a_fleet_if_logged_in()
    {
        $fleet = Fleet::factory()->create();

        $this->actingAs(User::factory()->create())
            ->deleteJson('/api/fleets/' . $fleet->id)
            ->assertOk();

        $this->assertDatabaseMissing('fleets', [
           'id' => $fleet->id
        ]);
    }

    public function test_i_cannot_delete_a_fleet_if_not_logged_in()
    {
        $fleet = Fleet::factory()->create();

        $this->deleteJson('/api/fleets/' . $fleet->id)
            ->assertStatus(401);

        $this->assertDatabaseHas('fleets', [
            'id' => $fleet->id
        ]);
    }

    public function test_i_cannot_delete_a_non_existent_fleet()
    {
        $this->actingAs(User::factory()->create())
            ->deleteJson('/api/fleets/222')
            ->assertNotFound();
    }
}
