<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fleet_armaments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('fleet_id');
            $table->unsignedMediumInteger('armament_id');
            $table->unsignedSmallInteger('qty');

            $table->foreign('fleet_id')->references('id')->on('fleets');
            $table->foreign('armament_id')->references('id')->on('armaments');


            $table->unique(['fleet_id', 'armament_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fleet_armaments');
    }
};
