<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Status;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Fleet>
 */
class FleetFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()->fleetName,
            'category_id' => function() {
                return Category::factory()->create();
            },
            'crew' => random_int(1000, 9999),
            'image_path' => $this->faker->image,
            'value' => $this->faker->randomFloat(2, 11, 11111),
            'status_id' => function() {
                return Status::factory()->create();
            },
        ];
    }
}
