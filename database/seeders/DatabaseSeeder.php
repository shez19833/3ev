<?php

namespace Database\Seeders;

use App\Models\Armament;
use App\Models\Category;
use App\Models\Fleet;
use App\Models\Status;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // @todo normally would use separate class seeders
        User::factory()->create([
            'email' => 'admin@me.com',
        ]);

        Category::factory()->count(2)->create();
        Status::factory()->count(2)->create();
        Armament::factory()->count(2)->create();
        Fleet::factory()->count(2)->create();

        $armaments = Armament::all(['id']);
        Fleet::all()->each(function ($fleet) use ($armaments) {
            $armaments->random(random_int(1, 2))->each(function ($armament) use ($fleet) {
                $fleet->armaments()->attach($armament->id, ['qty' => random_int(1, 10)]);
            });
        });

    }
}
