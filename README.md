## Test

Test was to create endpoints to add/update/show/list fleets

## Run

- clone the repository
- run composer install
- copy .env.example to .env
- run `php artisan key:generate`
- run `php artisan migrate --seed`
  - Note I have added a sqlite db for quicker startup time, please change your .env DB_CONNECTION and DB_DATABASE if you want to use mysql
  - run `touch database/database.sqlite` if you are ok to use sqlite BEFORE migrating
- run `php artisan serve`
- make sure the app url in .env corresponds to the url previous command gives you
- now you can make API requests through postman
- for authenticated routes, please run login request first to get your token
  - login details: admin@me.com / password

A sample JSON examples/docs can be found at: https://www.getpostman.com/collections/59b2af65c3d85bb9ccec
You would need to import it to POSTMAN 

## Tests

Tests can be run using `php artisan test`

## Considerations
- left some todos in code to show how I can improve it
- wasn't sure about some domain aspects so in real project, would have clarified naming better
- class was renamed to Category internally due to Class being a php keyword
- For adding a Fleet, I assumed all fields are required
- For searching i used category_id, status_id as opposed to name because in real life user would have an interface where they would have a dropdown and F/E will send ids to backend
- Tests are there but could have added few more tests

## Further improvements
- using SLUGS in the api endpoints instead of IDs
- Adding Services/Action Classes and put the logic there instead of in controller
- In the db I would probably add roles/permissions for user but since this api doesn't have normal logged users, I didnt go this far.

