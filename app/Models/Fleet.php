<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fleet extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'category_id',
        'crew',
        'image_path',
        'value',
        'status_id',
    ];

    public function getImageUrlAttribute()
    {
        return config('app.url') . '/storage/' . $this->image_path;
    }

    public function armaments()
    {
        return $this->belongsToMany(Armament::class, 'fleet_armaments')
            ->withPivot('qty');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function scopeSearch($q, $data)
    {
        if (empty($data)) {
            return $q;
        }

        if (isset($data['name'])) {
            $q->where('name', $data['name']);
        }

        if (isset($data['category_id'])) {
            $q->where('category_id', $data['category_id']);
        }

        if (isset($data['status_id'])) {
            $q->where('status_id', $data['status_id']);
        }

        return $q;
    }
}

