<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ImageUploadService
{
    // @todo possibly doing too much?
    public function upload(string $image)
    {
        $path = 'images/fleets/' . Str::random() . '.png';

        Storage::put($path, file_get_contents($image));

        return $path;
    }
}
