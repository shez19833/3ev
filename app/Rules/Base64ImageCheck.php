<?php

namespace App\Rules;

use Facades\Illuminate\Support\Str;
use Illuminate\Contracts\Validation\Rule;

// @todo write test
class Base64ImageCheck implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return base64_encode(base64_decode(explode(',', $value)[1], true)) === explode(',', $value)[1];
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'base64 param is not a valid base64 encoded string';
    }
}
