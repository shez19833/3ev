<?php

namespace App\Http\Requests;

use App\Rules\Base64ImageCheck;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

// @todo i could have done something fancy so I use same Request for both add/update
// to avoid duplicating the rules in two places..
class FleetUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required',
                Rule::unique('fleets')->ignore((int) $this->fleet->id)
            ],
            'category_id' => 'required|exists:categories,id',
            'crew' => 'required|numeric',
            'image' => [new Base64ImageCheck()],
            'value' => 'required|numeric',
            'status_id' => 'required|exists:statuses,id',
        ];
    }
}
