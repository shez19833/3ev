<?php

namespace App\Http\Requests;

use App\Rules\Base64ImageCheck;
use Illuminate\Foundation\Http\FormRequest;

class FleetStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:fleets',
            'category_id' => 'required|exists:categories,id',
            'crew' => 'required|numeric',
            'image' => ['required', new Base64ImageCheck()],
            'value' => 'required|numeric',
            'status_id' => 'required|exists:statuses,id',
        ];
    }
}
