<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\FleetStoreRequest;
use App\Http\Requests\FleetUpdateRequest;
use App\Http\Resources\FleetResource;
use App\Http\Resources\FleetResourceCollection;
use App\Models\Fleet;
use App\Services\ImageUploadService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FleetsController extends Controller
{
    public function __construct()
    {
        // @todo in a bigger project I would do this in routes files..
        $this->middleware('auth:sanctum')->except(['index', 'show']);
    }

    public function index(Request $request)
    {
        $fleets = Fleet::with('status')
            ->search($request->all())
            ->get();

        return FleetResourceCollection::collection($fleets);
    }

    public function show(Fleet $fleet)
    {
        $fleet->load('armaments', 'category', 'status');

        return new FleetResource($fleet);
    }

    public function store(FleetStoreRequest $request, ImageUploadService $imageUploadService)
    {
        Fleet::create($request->validated() + ['image_path' => $imageUploadService->upload($request->image)]);

        return $this->success(201);
    }

    public function update(FleetUpdateRequest $request, Fleet $fleet, ImageUploadService $imageUploadService)
    {
        // @todo a candidate for an action class so controller is a bit skinny as it doesnt need to know all
        // this.

        $data = $request->validated();

        if ($request->has('image')) {
            Storage::disk('public')->delete($fleet->image_path);
            $data['image_path'] = $imageUploadService->upload($request->image);
        }

        $fleet->update($data);

        return $this->success();
    }

    public function destroy(Fleet $fleet)
    {
        $fleet->delete();

        return $this->success();
    }
}
