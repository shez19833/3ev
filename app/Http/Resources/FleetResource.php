<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FleetResource extends JsonResource
{
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'class' => $this->category->name,
            'crew' => $this->crew,
            'image' => $this->image_url,
            'value' => $this->value,
            'status' => $this->status->name,
            'armament' => FleetArmamentResource::collection($this->armaments),

        ];
    }
}
