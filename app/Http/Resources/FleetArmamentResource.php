<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FleetArmamentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            // @todo i personally would have called it name, so everywhere we have 'name'
            // so we dont have to think if we called something title or name
            'title' => $this->name,
            'qty' => $this->pivot->qty,
        ];
    }
}
