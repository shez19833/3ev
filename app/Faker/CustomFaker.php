<?php

namespace App\Faker;

use Faker\Provider\Base;

class CustomFaker extends Base
{
    public function armament(): string
    {
        return static::randomElement([
            'Turbo Laser',
            'Ion Cannons',
            'Tractor Beam',
        ]);
    }

    public function category(): string
    {
        return static::randomElement([
            'Star Destroyer',
            'Deployment Pod',
            'Supply Ship',
            'Fast Frigate',
        ]);
    }

    public function fleetName(): string
    {
        return static::randomElement([
            'Devastator',
            'Red Five',
            'Green Star',
            'Blue Star',
        ]);
    }

    public function status(): string
    {
        return static::randomElement([
            'Operational',
            'Damaged',
            'Some Status',
            'Another Status'
        ]);
    }
}

